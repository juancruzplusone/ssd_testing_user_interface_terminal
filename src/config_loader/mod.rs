
use std::fs::File;
use std::io::Read;
use std::path::Path;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub pins: PinConfig,
    pub timers: TimerConfig,
    pub general: General
}

#[derive(Deserialize)]
pub struct PinConfig {
   pub relay_1_gpio_pin: u8,
   pub relay_2_gpio_pin: u8,
   pub ipc_1_status_gpio_pin: u8,
   pub ipc_2_status_gpio_pin: u8,
}

#[derive(Deserialize)]
pub struct TimerConfig {
    pub sleep_for_boot_timer_seconds: u64,
    pub sleep_until_power_off_timer_seconds: u64,
    pub sleep_after_power_down_timer_seconds: u64,
}

#[derive(Deserialize)]
pub struct General {
    pub max_cycles: u8,
}

fn load_config<P: AsRef<Path>>(path: P) -> Result<Config, Box<dyn std::error::Error>> {
    let mut config_string = String::new();
    let mut file = File::open(path)?;
    file.read_to_string(&mut config_string)?;
    let config = toml::from_str(&config_string)?;
    Ok(config)
}

pub fn get_config<P: AsRef<Path>>(config_path: P) -> Result<Config, Box<dyn std::error::Error>> {
    match load_config(config_path) {
        Ok(config) => {
            println!("Relay 1 GPIO Pin: {}", config.pins.relay_1_gpio_pin);
            println!("Relay 2 GPIO Pin: {}", config.pins.relay_2_gpio_pin);
            println!("IPC 1 Status GPIO Pin: {}", config.pins.ipc_1_status_gpio_pin);
            println!("IPC 2 Status GPIO Pin: {}", config.pins.ipc_2_status_gpio_pin);
            println!("Sleep for Boot Timer: {} seconds", config.timers.sleep_for_boot_timer_seconds);
            println!("Sleep Until Power Off Timer: {} seconds", config.timers.sleep_until_power_off_timer_seconds);
            println!("Sleep After Power Down Timer: {} seconds", config.timers.sleep_after_power_down_timer_seconds);
            Ok(config)
        },
        Err(e) => {
            eprintln!("Failed to read config: {}", e);
            Err(e)
        }
    }
}
