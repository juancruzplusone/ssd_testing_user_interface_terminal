mod config_loader;
use config_loader::{get_config, Config};
use rpi_interface::rpi_interface::{toggle_relay_pin_on_with_after_delay, toggle_relay_pin_off_with_after_delay, get_pin_state};


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum States {
    Running,
    Error,
    Completed,
}

struct TestData {
    power_cycle_count: u16,
    state: States,
    config: Config,
}

impl TestData {
    fn new(config: Config) -> Self {
        Self {
            power_cycle_count: 0,
            state: States::Running,  // Initial state is Running
            config,
        }
    }

    fn cycle_time_calculator(&self) -> u64 {
        let sleep_for_boot = self.config.timers.sleep_for_boot_timer_seconds;
        let sleep_until_power_off = self.config.timers.sleep_until_power_off_timer_seconds;
        let sleep_after_power_down = self.config.timers.sleep_after_power_down_timer_seconds;
        sleep_for_boot + sleep_until_power_off + sleep_after_power_down
    }

    fn total_test_time_calculator(&self) -> u64 {
        self.cycle_time_calculator() * self.config.general.max_cycles as u64
    }

    fn run(&mut self) {

        // Cut power on start
        toggle_relay_pin_on_with_after_delay(self.config.pins.relay_1_gpio_pin, 100);
        toggle_relay_pin_on_with_after_delay(self.config.pins.relay_2_gpio_pin, 100);

        while self.power_cycle_count < self.config.general.max_cycles && self.state == States::Running {
            println!("Cycle {}", self.power_cycle_count + 1);
            self.test_cycle();
            self.power_cycle_count += 1;
        }

        self.state = States::Completed;
        println!("Test completed after {} cycles", self.power_cycle_count);
    }

fn test_cycle(&mut self) {
        // Powering on the IPCs
        toggle_relay_pin_off_with_after_delay(self.config.pins.relay_1_gpio_pin, 100);
        toggle_relay_pin_off_with_after_delay(self.config.pins.relay_2_gpio_pin, 100);

        // Sleep while IPCs boot into OS
        std::thread::sleep(std::time::Duration::from_secs(self.config.timers.sleep_for_boot_timer_seconds));

        // Check pins status after they should have gone back high post-boot
        let mut ipc_1_state =  get_pin_state(self.config.pins.ipc_1_status_gpio_pin)
            .unwrap_or_else(|e| {
                println!("Failed to read IPC 1 pin state: {}", e);
                false // Assuming false to prevent further actions if error occurs
            });
        let mut ipc_2_state = get_pin_state(self.config.pins.ipc_2_status_gpio_pin)
            .unwrap_or_else(|e| {
                println!("Failed to read IPC 2 pin state: {}", e);
                false // Assuming false to prevent further actions if error occurs
            });

        if !ipc_1_state || !ipc_2_state {
            println!("Error detected after boot phase, status pins should be high but are low");

            println!("IPC1 State: {}", ipc_1_state);
            println!("IPC2 State: {}", ipc_2_state);
            self.state = States::Error;
            return;
        }
        else {
            // Sleep for a while to simulate the IPCs being on
            std::thread::sleep(std::time::Duration::from_secs(self.config.timers.sleep_until_power_off_timer_seconds));
        }

        ipc_1_state = get_pin_state(self.config.pins.ipc_1_status_gpio_pin)
            .unwrap_or_else(|e| {
                println!("Failed to read IPC 1 pin state: {}", e);
                false // Assuming false to prevent further actions if error occurs
            });

        ipc_2_state = get_pin_state(self.config.pins.ipc_2_status_gpio_pin)
            .unwrap_or_else(|e| {
                println!("Failed to read IPC 2 pin state: {}", e);
                false // Assuming false to prevent further actions if error occurs
            });

        if !ipc_1_state || !ipc_2_state {
            println!("Error detected on IPC, status pins should be high but are low");

            println!("IPC1 State: {}", ipc_1_state);
            println!("IPC2 State: {}", ipc_2_state);
            self.state = States::Error;
            return;
        }
        else {

            // Now, assuming the IPCs are stable, they will be turned off.
            toggle_relay_pin_on_with_after_delay(self.config.pins.relay_1_gpio_pin, 100);
            toggle_relay_pin_on_with_after_delay(self.config.pins.relay_2_gpio_pin, 100);

            // Sleep to allow SSD to discharge
            std::thread::sleep(std::time::Duration::from_secs(self.config.timers.sleep_after_power_down_timer_seconds));
        }
    }
}

use std::time::{Duration, SystemTime};
use std::time::UNIX_EPOCH;

fn main() {
    println!("Welcome to the SSD testing experience!");

    println!("----------------------------------");
    println!("Loading configuration...");
    let config_path = "config.toml";
    let config = match config_loader::get_config(config_path) {
        Ok(cfg) => cfg,
        Err(e) => {
            println!("Failed to load configuration: {}", e);
            return;
        },
    };

    let mut test_data = TestData::new(config);

    println!("----------------------------------");
    let total_test_duration = test_data.total_test_time_calculator();
    println!("Estimated test time: {} minutes", total_test_duration / 60);
    
    // Get real-time and add estimated test time
    let start_time = SystemTime::now();
    let end_time = start_time + Duration::from_secs(total_test_duration);

    // Optionally, convert end_time to a readable format
    match end_time.duration_since(UNIX_EPOCH) {
        Ok(n) => println!("Estimated end time (epoch seconds): {}", n.as_secs()),
        Err(_) => println!("SystemTime before UNIX EPOCH!"),
    }

    // Display end time in human-readable format
    match end_time.elapsed() {
        Ok(elapsed) => {
            // elapsed is the duration since end_time, so we calculate the remaining time
            let remaining_time = Duration::from_secs(total_test_duration) - elapsed;
            println!("Time remaining: {} seconds", remaining_time.as_secs());
        },
        Err(e) => {
            let datetime = chrono::DateTime::<chrono::Utc>::from(end_time);
            println!("Estimated end time: {}", datetime.to_rfc2822());
        }
    }

    println!("----------------------------------");
    // Countdown to start 3 sec
    for i in (0..3).rev() {
        println!("Starting in {}...", (i + 1));
        std::thread::sleep(std::time::Duration::from_secs(1));
    }
    println!("----------------------------------");

    test_data.run();
}
